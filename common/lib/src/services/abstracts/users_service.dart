import '../../abstracts/service.dart';

abstract class UsersService extends Service {
  fetchItems();
}
